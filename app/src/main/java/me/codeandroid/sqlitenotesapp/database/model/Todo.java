package me.codeandroid.sqlitenotesapp.database.model;

public class Todo {
    public static final String TABLE_NAME = "todos";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NOTE = "todo";
    public static final String COLUMN_TIMESTAMP = "timestamp";

    private int id;
    private String todo;
    private String timestamp;


    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NOTE + " TEXT,"
                    + COLUMN_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";

    public Todo() {
    }

    public Todo(int id, String todo, String timestamp) {
        this.id = id;
        this.todo = todo;
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public String getTodo() {
        return todo;
    }

    public void setTodo(String note) {
        this.todo = note;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
