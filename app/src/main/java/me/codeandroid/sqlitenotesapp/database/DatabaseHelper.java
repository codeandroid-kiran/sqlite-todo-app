package me.codeandroid.sqlitenotesapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import me.codeandroid.sqlitenotesapp.database.model.Todo;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "todos_db";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }




    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Todo.CREATE_TABLE); //create Table Statement provided in Todo.class
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //drops table if we already have it
        db.execSQL("DROP TABLE IF EXISTS " + Todo.TABLE_NAME);

        //creates again after dropping
        onCreate(db);

    }

    public long insertTodo(String todo) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(Todo.COLUMN_NOTE, todo);

        // insert row
        long id = db.insert(Todo.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public Todo getTodo(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Todo.TABLE_NAME,
                new String[]{Todo.COLUMN_ID, Todo.COLUMN_NOTE, Todo.COLUMN_TIMESTAMP},
                Todo.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        Todo todo = new Todo(
                cursor.getInt(cursor.getColumnIndex(Todo.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Todo.COLUMN_NOTE)),
                cursor.getString(cursor.getColumnIndex(Todo.COLUMN_TIMESTAMP)));

        // close the db connection
        cursor.close();

        return todo;
    }

    public List<Todo> getAllTodos() {
        List<Todo> todos = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + Todo.TABLE_NAME + " ORDER BY " +
                Todo.COLUMN_TIMESTAMP + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Todo todo = new Todo();
                todo.setId(cursor.getInt(cursor.getColumnIndex(Todo.COLUMN_ID)));
                todo.setTodo(cursor.getString(cursor.getColumnIndex(Todo.COLUMN_NOTE)));
                todo.setTimestamp(cursor.getString(cursor.getColumnIndex(Todo.COLUMN_TIMESTAMP)));

                todos.add(todo);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return todos list
        return todos;
    }

    public int getTodoCount() {
        String countQuery = "SELECT  * FROM " + Todo.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public int updateTodo(Todo todo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Todo.COLUMN_NOTE, todo.getTodo());

        // updating row
        return db.update(Todo.TABLE_NAME, values, Todo.COLUMN_ID + " = ?",
                new String[]{String.valueOf(todo.getId())});
    }

    public void deleteTodo(Todo todo) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Todo.TABLE_NAME, Todo.COLUMN_ID + " = ?",
                new String[]{String.valueOf(todo.getId())});
        db.close();
    }
}
